# Choosing an strategy

> ### TODO
>
> Before choosing a GitHub alternative, some people need to choose a strategy:
>
> - Discuss the options, such as:
>   - Total Evacuation
>   - Evacuation, but maintain presence on GitHub to be able to contribute to projects that aren't ready to leave yet
>   - Evacuation, but maintain shell repos on GitHub for:
>      - Issues if you didn't migrate them over
>      - forwarding address. Ask contributors to come over with you.
>
> If the decision tree is large or complicated, consider creating a **flow chart**, or something **interactive**, that makes it easier for people of all sorts of circumstances find the path best for them.

