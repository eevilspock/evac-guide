> ## NOTICE: Work in Progress
>
> This guide is a work in progress. We will remove this notice when we feel the guide is reliable and complete enough for you to use for decisions. For now take it with a grain of salt.
>
> When ready, it will be published at www.upend.org/github.
>
> **Want to make this guide better or more complete?** If you any feedback or ideas, [let us know](CONTRIBUTING.md#reporting-an-issue) or [make a change directly](CONTRIBUTING.md#editing-the-guide).



# GitHub Evacuation Guide

If you're here you probably already know [why it's important to evacuate](why-evacuate.md).

This guide will walk you through all the steps and cover all the bases. Follow the links in each step for details:

1. [Let others know](covering-all-your-bases.md#letting-others-know-in-advance) that you of your plans. You'll want your users and contributors to know what's coming. You'll also spread the word about this movement, and provide moral support for others thinking about evacuating.
2. [Decide on a strategy](choosing-a-strategy.md).
3. [Pick a GitHub alternative](picking-a-github-alternative.md). Our [Score Card](open-source-vcs-scorecard.md) makes it easier.
4. [Migrate your repos](migrating-your-repos.md). We provide both instructions and links to [migration tools](migration-tools.md).
5. [Leave a forwarding address](covering-all-your-bases.md#leave-a-forwarding-address).
6. Join [the movement](https://gitlab.com/upend/github/evac-movement).




