# Choosing an alternative to GitHub.

> ### TODO
>
> guide the reader in choosing a GitHub alternative.
>
> - be objective, present facts, do not make the decision for them.
> - Discuss the issues.
> - Link to the Score Card. 



This guide is being researched and written. We will complete it as soon as possible, long before Microsoft takes over. You can star this repo, or [this repo on GitHub](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT), [follow us on Twitter](https://twitter.com/upend_org), or email github.evacuees@upend.org to be notified up updates. 

If you can't wait, here are some resources you can use for your own research:

- https://en.wikipedia.org/wiki/Comparison_of_source_code_hosting_facilities
- https://www.git-tower.com/blog/git-hosting-services-compared/
- https://git.wiki.kernel.org/index.php/GitHosting

If you learn something in your research or choice that would be useful for other evacuees, good or bad, please let us know! You can share it via a pull/merge request, by opening an issue, or via github.evacuees@upend.org.

