# Covering all your bases

> ### TODO
>
> anything that is a good thing for people to do that doesn't fit into the other parts of this guide should go here. If it gets too large, we can break it up later. But let's just get everything that people should do down somewhere.
>
> the content below is just an off-hand first cut. We need to think through all of the things people should do to make their migration smoother as well as make things easier for users, contributors and other followers of the account or repo.



## Letting others know in advance

If you have a lot of users or contributors, you will want to let them know in advance. 

> ### TODO
>
> Are there may be things that the maintainer or the contributors have to do in prep of a move? Maybe clean up obsolete issues and pull requests? what else?







## Leave a forwarding address



### Repo forwarding address

> ### TODO
>
> The steps below are an off-hand first cut and need to be completed, polished and published.



If you would like to support the [GitHub Evacuation Movement](https://gitlab.com/upend/github/evac-movement), you can:

- put [this logo](project_logo.png) and the message below in your README:

    ```
    # This Repo Has Moved
    In light of the Microsoft assimilation of GitHub, this repo has moved...
    
    It's new home is x:
    
    You can update your git remote as follows:
    
    ​```
    git remote xxxxx
    ​```
    ```
     

- tag your repo with `github evacuee` or `github wannabe evacuee`  depending on your status

- link to the Evacuation Project in your README.







### Account forwarding address

> ### TODO
>
> steps for leaving a forwarding address


If you would like to support the [GitHub Evacuation Movement](https://gitlab.com/upend/github/evac-movement), you can:

- you can replace your avatar with [this](project_logo.png)
- link to the Evacuation Project in your profile.





## Want to Totally Break with GitHub?

> ### TODO
>
> do we want to write this section?
>
> e.g. Strip your profile of all personal information (See https://github.com/Apollia)





## Examples

> ### TODO
>
> privide links or screenshots of accounts and repos that have done the above steps.

